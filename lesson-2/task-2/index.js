/**
 * Задание 2
 *
 * Объявите две переменные: admin и name.
 * Запишите строку "John" в переменную name.
 * Скопируйте значение из переменной name в admin.
 * Выведите на экран значение admin, используя функцию console.log (должна показать John).
 */

// РЕШЕНИЕ

const name = 'John';
const admin = name;

console.log('Admin name:', admin);