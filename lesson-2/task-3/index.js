/**
 * Задание 3
 *
 * Переписать код используя короткую форму записи и исправить ошибки.
 */

// console.log() оставил специально)

let myValue = 2;
console.log('myValue now:', myValue);

myValue = myValue + 2;
console.log('myValue now:', myValue);

myValue = ++myValue;
console.log('myValue now:', myValue);

myValue = myValue - 10;
console.log('myValue now:', myValue);

const data = myValue ** 3;
console.log('data now:', data);