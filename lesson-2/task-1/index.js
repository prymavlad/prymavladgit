/**
 * Задание 1
 *
 * Написать скрипт сложения, вычитания, умножения и деления двух чисел.
 * Результат каждой операции должен быть записан в переменную и выведен в консоль.
 *
 * Пример: sum1 = 1
 *         sum2 = 2
 *         результуть сложения: 3
 *         результуть вычитания: -1
 *         результуть умножения: 2
 *         результуть деления: 0.5
 */
const sum1 = prompt('Введите первое число:'); // ЗАПРЕЩЕНО МЕНЯТЬ
const sum2 = prompt('Введите второе число:'); // ЗАПРЕЩЕНО МЕНЯТЬ

// РЕШЕНИЕ

const result1 = Number(sum1) + Number(sum2);
const result2 = Number(sum1) - Number(sum2);
const result3 = Number(sum1) * Number(sum2);
const result4 = Number(sum1) / Number(sum2);

console.log('Резульат сложения:', result1);
console.log('Резульат вычитания:', result2);
console.log('Резульат умножения:', result3);
console.log('Резульат деления:', result4);