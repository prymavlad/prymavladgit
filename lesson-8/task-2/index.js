/**
 * Доработать форму из 1-го задания.
 *
 * Добавить обработчик сабмита формы.
 *
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 *
 * Обязательно!
 * 1. При сабмите формы страница не должна перезагружаться
 * 2. Генерировать ошибку если пользователь пытается сабмитить форму с пустыми или содержащими только пробел(ы) полями.
 * 3. Если поля формы заполнены и пользователь нажимает кнопку Вход → вывести в консоль объект следующего вида
 * {
 *  email: 'эмейл который ввёл пользователь',
 *  password: 'пароль который ввёл пользователь',
 *  remember: 'true/false'
 * }
 */

// РЕШЕНИЕ

const form = document.querySelector('#form');
const emailDiv = document.createElement('div');
const emailLabel = document.createElement('label');
const emailInput = document.createElement('input');
const passwordDiv = document.createElement('div');
const passwordLabel = document.createElement('label');
const passwordInput = document.createElement('input');
const checkboxDiv = document.createElement('div');
const checkboxLabel = document.createElement('label');
const checkboxInput = document.createElement('input');
const createButton = document.createElement('button');

emailDiv.setAttribute('class', 'form-group');
emailLabel.setAttribute('for', 'email');
emailInput.setAttribute('type', 'email');
emailInput.setAttribute('class', 'form-control');
emailInput.setAttribute('id', 'email');
emailInput.setAttribute('placeholder', 'Введите свою электропочту');

passwordDiv.setAttribute('class', 'form-group');
passwordLabel.setAttribute('for', 'password');
passwordInput.setAttribute('type', 'password');
passwordInput.setAttribute('class', 'form-control');
passwordInput.setAttribute('id', 'password');
passwordInput.setAttribute('placeholder', 'Введите пароль');

checkboxDiv.setAttribute('class', 'form-group form-check');
checkboxLabel.setAttribute('class', 'form-check-label');
checkboxLabel.setAttribute('for', 'exampleCheck1');
checkboxInput.setAttribute('type', 'checkbox');
checkboxInput.setAttribute('class', 'form-check-input');
checkboxInput.setAttribute('id', 'exampleCheck1');

createButton.setAttribute('type', 'submit');
createButton.setAttribute('class', 'btn btn-primary');

form.append(emailDiv);
emailDiv.append(emailLabel);
emailLabel.innerHTML = 'Электропочта';
emailDiv.append(emailInput);

form.append(passwordDiv);
passwordDiv.append(passwordLabel);
passwordLabel.innerHTML = 'Пароль';
passwordDiv.append(passwordInput);

form.append(checkboxDiv);
checkboxDiv.append(checkboxInput);
checkboxDiv.append(checkboxLabel);
checkboxLabel.innerHTML = 'Запомнить меня';

form.append(createButton);
createButton.innerHTML = 'Вход';


form.addEventListener('submit', function (e){
    e.preventDefault();

    const email = e.target[0].value;
    const password = e.target[1].value;
    const remember = e.target[2].checked;

    if(email.trim() === '') {
        throw new Error('Вы не ввели емейл!');
    }

    if(password.trim() === '') {
        throw new Error('Вы не ввели пароль!');
    }

    return console.log({
        email: email,
        password: password,
        remember: remember
    });
});