/**
 * Создать форму динамически при помощи JavaScript.
 *
 * В html находится пример формы которая должна быть сгенерирована.
 *
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 *
 * Обязательно!
 * 1. Для генерации элементов обязательно использовать метод document.createElement
 * 2. Для установки атрибутов элементам обязательно необходимо использовать document.setAttribute
 * 3. Всем созданным элементам необходимо добавить классы как в разметке
 * 4. После того как динамическая разметка будет готова необходимо удалить код в HTML который находится между комментариями
 */

// РЕШЕНИЕ

const form = document.querySelector('#form');
const emailDiv = document.createElement('div');
const emailLabel = document.createElement('label');
const emailInput = document.createElement('input');
const passwordDiv = document.createElement('div');
const passwordLabel = document.createElement('label');
const passwordInput = document.createElement('input');
const checkboxDiv = document.createElement('div');
const checkboxLabel = document.createElement('label');
const checkboxInput = document.createElement('input');
const createButton = document.createElement('button');

emailDiv.setAttribute('class', 'form-group');
emailLabel.setAttribute('for', 'email');
emailInput.setAttribute('type', 'email');
emailInput.setAttribute('class', 'form-control');
emailInput.setAttribute('id', 'email');
emailInput.setAttribute('placeholder', 'Введите свою электропочту');

passwordDiv.setAttribute('class', 'form-group');
passwordLabel.setAttribute('for', 'password');
passwordInput.setAttribute('type', 'password');
passwordInput.setAttribute('class', 'form-control');
passwordInput.setAttribute('id', 'password');
passwordInput.setAttribute('placeholder', 'Введите пароль');

checkboxDiv.setAttribute('class', 'form-group form-check');
checkboxLabel.setAttribute('class', 'form-check-label');
checkboxLabel.setAttribute('for', 'exampleCheck1');
checkboxInput.setAttribute('type', 'checkbox');
checkboxInput.setAttribute('class', 'form-check-input');
checkboxInput.setAttribute('id', 'exampleCheck1');

createButton.setAttribute('type', 'submit');
createButton.setAttribute('class', 'btn btn-primary');

form.append(emailDiv);
emailDiv.append(emailLabel);
emailLabel.innerHTML = 'Электропочта';
emailDiv.append(emailInput);

form.append(passwordDiv);
passwordDiv.append(passwordLabel);
passwordLabel.innerHTML = 'Пароль';
passwordDiv.append(passwordInput);

form.append(checkboxDiv);
checkboxDiv.append(checkboxInput);
checkboxDiv.append(checkboxLabel);
checkboxLabel.innerHTML = 'Запомнить меня';

form.append(createButton);
createButton.innerHTML = 'Вход';