/**
 * Доработайте функцию что бы она возвращала объект из переданного вложенного массива
 *
 * Фукнция принимает 1 аргумента
 * 1. Массив из массивов который содержит 2 элемента — [ [ element1, element2 ] ]
 *
 * ЗАПРЕЩЕНО ИСПОЛЬЗОВАТЬ ВСТРОЕННЫЙ МЕТОД Object.fromEntries
 *
 * Обратите внимание!
 * 1. Генерировать ошибку если второй элемент вложенного массива не число, не строка или не null
 * 2. Обязательно использовать деструктуризацию при извлечении элементов массива
 * 3. Если в качестве второго аргумента был передан массив вида [ [ element1, element2 ] ], то его так же нужно преобразовать в объект
 * 4. Для перебора массива можно воспользоваться циклом for..of.
 */

const fromEntries = (entries) => {
    const obj = {};

    for (const el of entries) {

        let [key, value] = el;

        if(Array.isArray(value)){

            let [childKey, childValue] = value[0];
            let secondObj = {};
            secondObj[childKey] = childValue;
            obj[key] = secondObj;


        } else if(!isNaN(value) || typeof value === 'string') {

            obj[key] = value;

        } else {

            throw new Error('Ошибка, проверьте вводные данные!');

        }
    }

    return obj;
};

// console.log(fromEntries([['name', 'John'], ['age', 35], ['type', null], ['type2', NaN], ['type3', {}], ['type3', [['city', 'New  York']]]]));
console.log(fromEntries([['name', 'John'], ['age', 35]])); // { name: 'John', age: 35 }
console.log(fromEntries([['name', 'John'], ['address', [['city', 'New  York']]]])); // { name: 'John', address: { city: 'New  York' } }

