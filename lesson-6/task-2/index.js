/**
 * Задача 2.
 *
 * Создайте функцию `f`, которая возвращает сумму всех переданных числовых аргументов.
 *
 * Условия:
 * - Функция должна принимать любое количество аргументов;
 * - Генерировать ошибку, если в качестве любого входного аргумента было предано не число.
 */

// РЕШЕНИЕ

function f() {

    let argumentsSum = 0;

    for (let i = 0; i<arguments.length; i++) {

        if(!isNaN(arguments[i]) && typeof arguments[i] === 'number'){

            argumentsSum += arguments[i];

        } else {

            throw new Error(`Аргумент ${arguments[i]} не число! проверьте переданные параметры!`);

        }
    }

    return argumentsSum;
}

console.log(f(1, 1, 1, 2, 1, 1, 1, 1)); // 9
console.log(f(1, 1, 1, 12, NaN)); // 9
console.log(f(1, 1, 5, 98, 'string')); // 9
