/**
 * Задача 2.
 *
 * Напишите скрипт, который проверяет идентичны ли массивы.
 * Если массивы полностью идентичны - в переменную flag присвоить значение true,
 * иначе - false.
 *
 * Пример 1: const arr1 = [1, 2, 3];
 *           const arr2 = [1, '2', 3];
 *           flag -> false
 *
 * Пример 2: const arr1 = [1, 2, 3];
 *           const arr2 = [1, 2, 3];
 *           flag -> true
 *
 * Пример 3: const arr1 = [];
 *           const arr2 = arr1;
 *           flag -> true
 *
 * Пример 4: const arr1 = [];
 *           const arr2 = [];
 *           flag -> true
 *
 * Условия:
 * - Обязательно проверять являются ли сравниваемые структуры массивами;
 *
 */

const arr1 = [1, 2, 3];
const arr2 = [1, '2', 3];
// const arr1 = [1, 2, 3];
// const arr2 = [1, 2, 3];
// const arr1 = [];
// const arr2 = arr1;
// const arr1 = [];
// const arr2 = [];
// const arr1 = [1, 2, 3];
// const arr2 = [1, 2, 3, 4];
let flag = '';

// РЕШЕНИЕ

if(!Array.isArray(arr1) || !Array.isArray(arr2)) {

    flag = false;

} else {

    flag = compareArr(arr1, arr2);

}

function compareArr(array1, array2) {

    if(array1.length !== array2.length){

        return false;

    } else {

        for (let i = 0; i < array1.length; i++) {

            if(array1[i] !== array2[i]) {

                return false;

            }

        }

    }

    return true;

}

console.log(flag);