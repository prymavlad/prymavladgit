/*
 * Задача 3.
 *
 * Дописать требуемый функционал что бы код работал правильно.
 *
 * Необходимо проверить длину строки в переменной string.
 * Если она превосходит maxLength – заменяет конец string на ... таким образом, чтобы её длина стала равна maxLength.
 * В консоль должна вывестись (при необходимости) усечённая строка.
 *
 * Пример 1: string -> 'Вот, что мне хотелось бы сказать на эту тему:'
 *         maxLength -> 21
 *         result -> 'Вот, что мне хотел...'
 *
 * Пример 2: string -> 'Вот, что мне хотелось'
 *         maxLength -> 100
 *         result -> 'Вот, что мне хотелось'
 *
 * Условия:
 * - Переменная string должна обладать типом string;
 * - Переменная maxLength должна обладать типом number.
 *
 */
const string = prompt('Введите строку, которую нужно сократить:'); // ИЗМЕНЯТЬ ЗАПРЕЩЕНО
const maxLength = prompt('Введите максимальную длинну строки:'); // ИЗМЕНЯТЬ ЗАПРЕЩЕНО

// РЕШЕНИЕ

const maxLengthNumber = Number(maxLength);

if(typeof string === 'string' && Number.isInteger(maxLengthNumber) ){

    const endSumbol = '...';
    const new_string = string.slice(0, maxLength - endSumbol.length);

    if(string.length > (maxLength - endSumbol.length)) {
        console.log(`${new_string}${endSumbol}`);
    } else {
        console.log(new_string);
    }
}