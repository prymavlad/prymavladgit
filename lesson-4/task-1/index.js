/*
 * Задача 1.
 *
 * Дописать требуемый функционал что бы код работал правильно.
 *
 * Необходимо преобразовать первый символ строки в заглавный и вывести эту строку в консоль.
 * Пример 1: привет -> Привет
 *
 * Пример 2: 222 -> Error.
 *
 * Условия:
 * - Необходимо проверить что введенный текст не число иначе выводить ошибку в консоль.
 */

const str = prompt('Введите любую строку:'); // ИЗМЕНЯТЬ ЗАПРЕЩЕНО

// РЕШЕНИЕ
const toNumber = Number(str);

if(!Number.isInteger(toNumber) ) {
    const new_string = str[0].toUpperCase() + str.slice(1);

    console.log(new_string);
} else {
    console.log('Error');
}
