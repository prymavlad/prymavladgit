function bestBrowser(companiesArr) {

    const maxMarketShare = companiesArr.reduce(function(prev, current) {

        if (+current.marketShare > +prev.marketShare) {
            return current;
        } else {
            return prev;
        }

    });

    const { name, company, marketShare } = maxMarketShare;
    const results = `<span>"Самый востребованный браузер это ${name} от компании ${company} с процентом использования ${marketShare}%"</span>`;

    console.log(results);

}

export { bestBrowser };