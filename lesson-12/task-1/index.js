/**
 * Примерные данные для заполнения, вы можете использовать свои данные.
 *  Имя: "Firefox",   Компания: "Mozilla",        Процент: "8.01%"
 *  Имя: "Chrome",    Компания: "Google",         Процент: "68.26%"
 *  Имя: "Edge",      Компания: "Microsoft",      Процент: "6.67%"
 *  Имя: "Opera",     Компания: "Opera Software", Процент: "1.31%"
 *
 */

const form = document.querySelector('#form');
const browserName = document.querySelector('#form input[name="browser"]');
const companyName = document.querySelector('#form input[name="company"]');
const percentValue = document.querySelector('#form input[name="percent"]');
const submitBtn = document.querySelector('#form input[type="submit"]');
const resultsBtn = document.querySelector('.results button');
const resultsContainer = document.querySelector('#result');
const companies = [];

submitBtn.setAttribute("disabled", "disabled");

document.addEventListener('input', () => validation(browserName, companyName, percentValue));
form.addEventListener('submit', () => saveInfo(event));
resultsBtn.addEventListener('click', () => bestBrowser(companies))

function saveInfo (e) {

    e.preventDefault();

    const singleBrowser = {};
    const formData = new FormData(e.target);

    const browser = formData.get('browser');
    const company = formData.get('company');
    const percent = formData.get('percent');

    singleBrowser['name'] = browser;
    singleBrowser['company'] = company;
    singleBrowser['marketShare'] = percent;

    companies.push(singleBrowser);
    form.reset();

console.log(companies);
}

function validation(browser, company, percent) {

    if(browser.value === '' || typeof browser.value !== 'string' || browser.value.length < 3) {

        browser.classList.add('error');
        submitBtn.setAttribute("disabled", "disabled");
        return;

    } else {
        browser.classList.remove('error');
        submitBtn.removeAttribute("disabled");
    }

    if(company.value === '' || typeof company.value !== 'string' || company.value.length < 3) {

        company.classList.add('error');
        submitBtn.setAttribute("disabled", "disabled");
        return;

    } else {
        company.classList.remove('error');
        submitBtn.removeAttribute("disabled");
    }

    if(percent.value === '' || (Math.sign(percent.value) !== 1 && Math.sign(percent.value) !== 0) || percent.value > 100) {

        percent.classList.add('error');
        submitBtn.setAttribute("disabled", "disabled");
        return;

    } else {
        percent.classList.remove('error');
        submitBtn.removeAttribute("disabled");
    }

}

function bestBrowser(companiesArr) {

    if(companiesArr.length === 0){

        resultsContainer.innerHTML = `<span style="color: red;">"Недостаточно данных"</span>`;

    } else if (companiesArr.length === 1) {

        const { name, company, marketShare } = companiesArr[0];
        resultsContainer.innerHTML = `"Самый востребованный браузер это ${name} от компании ${company} с процентом использования ${marketShare}%"`;

    } else {

        const maxMarketShare = companiesArr.reduce(function(prev, current) {

            if (+current.marketShare > +prev.marketShare) {
                return current;
            } else {
                return prev;
            }

        });

        const { name, company, marketShare } = maxMarketShare;
        resultsContainer.innerHTML = `<span>"Самый востребованный браузер это ${name} от компании ${company} с процентом использования ${marketShare}%"</span>`;

    }


}